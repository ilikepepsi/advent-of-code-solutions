#!/usr/bin/python

import argcomplete, argparse
import sys

from pathlib import Path


# Create the parser
parser = argparse.ArgumentParser(description="Specify the input file.")

# Add the arguments
parser.add_argument('InputFile',
                    metavar='inputFile',
                    type=str,
                    help='the input file')

# Add autocomplete
argcomplete.autocomplete(parser)

# Execute the parse_args() method
args = parser.parse_args()

input_file_path = Path(args.InputFile)

if not input_file_path.exists:
    print(f'File does not exist at: {input_file_path.resolve()}')
    sys.exit()

with open(input_file_path.resolve(), 'r') as f:
    
    largerThanPreviousMeasurementsCount = 0
    lastLine = None
    
    for line in f:
        # first iteration
        if lastLine is None:
            lastLine = int(line)
            continue
        
        if int(line) > lastLine:
            largerThanPreviousMeasurementsCount += 1 
            
        lastLine = int(line)
            
    print(f'Larger than previous count: {largerThanPreviousMeasurementsCount}')
        
        

