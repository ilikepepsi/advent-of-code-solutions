#!/usr/bin/python

import argcomplete, argparse
import sys

from pathlib import Path

def sum(values: list):
    sum = 0
    for v in values:
        sum += int(v)
    return sum


def inverse(bits: list):
    inverse_s = ''

    for i in bits:
        if i == '0':
            inverse_s += '1'
        else:
            inverse_s += '0'

    return inverse_s


def list_to_string(chars: list):
    result = ''

    for c in chars:
        result += c

    return result


def binary_to_decimal(bits: str):
    result = 0
    exp = 0
    for b in bits[::-1]:
        result += int(b) * 2 ** exp
        exp += 1

    return result

# Create the parser
parser = argparse.ArgumentParser(description="Specify the input file.")

# Add the arguments
parser.add_argument('InputFile',
                    metavar='inputFile',
                    type=str,
                    help='the input file')

parser.add_argument('-w',
                    '--wordLength',
                    type=int,
                    help='Length of the binary word in bits')

# Add autocomplete
argcomplete.autocomplete(parser)

# Execute the parse_args() method
args = parser.parse_args()

input_file_path = Path(args.InputFile)

if not input_file_path.exists:
    print(f'File does not exist at: {input_file_path.resolve()}')
    sys.exit()

with open(input_file_path.resolve(), 'r') as f:

    lines = f.readlines()
    
    if args.wordLength is None:
        word_length = len(lines[0].strip().replace('\r', '').replace('\n', ''))
    else:
        word_length = args.wordLength
    
    if word_length == 0:
        sys.exit("Binary word length should be at least 1 bit.")
    
    bitColumns = list(range(word_length))
    gamma_input = list(range(word_length))

    for i in range(word_length):
        bitColumns[i] = [lines[l][i] for l in range(len(lines))]

        gamma_input[i] = sorted(bitColumns[i])[int(len(lines)/2)]

    gamma = list_to_string(gamma_input)
    epsilon = inverse(gamma)

    print(f'Gamma and Epsilon values: \nGamma:\t\t{gamma}\nEpsilon:\t{epsilon}')

    print(f'Result:\t\t{binary_to_decimal(gamma) * binary_to_decimal(epsilon)}')