#!/usr/bin/python

from os import minor
import argcomplete, argparse
import sys

from pathlib import Path

def sum(values: list) -> int:
    sum = 0
    for v in values:
        sum += int(v)
    return sum


def inverse(bits: list) -> str:
    inverse_s = ''

    for i in bits:
        if i == '0':
            inverse_s += '1'
        else:
            inverse_s += '0'

    return inverse_s


def flip_bit(bit :str) ->str:
    if bit == '0':
        return '1'
    else:
        return '0'
        

def list_to_string(chars: list) -> str:
    result = ''

    for c in chars:
        result += c

    return result


def binary_to_decimal(bits: str) -> int:
    result = 0
    exp = 0
    for b in bits[::-1]:
        result += int(b) * 2 ** exp
        exp += 1

    return result


def find_most_common_bit(bits: list) -> str:
    zero_count = 0
    one_count = 0
    
    for b in bits:
        if int(b) == 0:
            zero_count += 1
        else:
            one_count += 1
            
    if one_count >= zero_count:
        return '1'
    else:
        return '0'
    
    
def find_least_common_bit(bits: list) -> str:
    zero_count = 0
    one_count = 0
    
    for b in bits:
        if int(b) == 0:
            zero_count += 1
        else:
            one_count += 1
            
    if zero_count > one_count:
        return '1'
    else:
        return '0'
    

def is_equal(value :str, majority :str) -> bool:
    return value == majority


def bit_criteria_matcher(data :list, word_length :int, bit_criteria_selector_method :any) -> str:
    """This method is used to sort out all data-elements not matching the bit-criteria until only 1 is left

    Args:
        data (list): The list of data-elements
        bit_criteria (list): The list of bit-criterias

    Returns:
        str: The remaining data element
    """

    bitColumns = list(range(word_length))

    # Create for-loop to iterate over bit positions
    for i in range(word_length):
        # Iterate over all data-elements (l in lines) and store the i-th values as list into the buffer
        bitColumns[i] = [element[i] for element in data]

        # Find the most common element for the current position
        bit_criteria = bit_criteria_selector_method(bitColumns[i])

        data = list(filter(lambda x: is_equal(x[i], bit_criteria), data))
        
        if len(data) == 1:
            break
        
    return data.pop()

# Create the parser
parser = argparse.ArgumentParser(description="Specify the input file.")

# Add the arguments
parser.add_argument('InputFile',
                    metavar='inputFile',
                    type=str,
                    help='the input file')

parser.add_argument('-w',
                    '--wordLength',
                    type=int,
                    help='Length of the binary word in bits')

# Add autocomplete
argcomplete.autocomplete(parser)

# Execute the parse_args() method
args = parser.parse_args()

input_file_path = Path(args.InputFile)

if not input_file_path.exists:
    print(f'File does not exist at: {input_file_path.resolve()}')
    sys.exit()

with open(input_file_path.resolve(), 'r') as f:

    # Read input data as list of data-elements
    lines = f.readlines()
    
    # Sanitize input -> remove '\r','\n' and ' '
    for i in range(len(lines)):
        lines[i] = lines[i].strip().replace('\r', '').replace('\n', '')
    
    # Copy the input data into lists used for bit-criteria matching
    majority = lines[:]
    minority = lines[:]
    
    # If the word length parameter is not given try to figure it out 
    if args.wordLength is None:
        # Take the first line and remove non-printable characters
        word_length = len(lines[0])
    else:
        # Take the value given by parameter
        word_length = args.wordLength
    
    # Test for invalid conditions
    if word_length == 0:
        # return error
        sys.exit("Binary word length should be at least 1 bit.")
    
    oxygen_generator_rating_str = bit_criteria_matcher(majority, word_length, find_most_common_bit)
    co2_scrubber_rating_str = bit_criteria_matcher(minority, word_length, find_least_common_bit)

    print(f'Oxygen generator rating: {oxygen_generator_rating_str}')
    print(f'CO2 scrubber rating: {co2_scrubber_rating_str}')
    
    print(f'Result: {binary_to_decimal(oxygen_generator_rating_str) * binary_to_decimal(co2_scrubber_rating_str)}')