#!/usr/bin/python

import argcomplete, argparse
import sys

from pathlib import Path

def sum(values: list):
    sum = 0
    for v in values:
        sum += int(v)
    return sum

# Create the parser
parser = argparse.ArgumentParser(description="Specify the input file.")

# Add the arguments
parser.add_argument('InputFile',
                    metavar='inputFile',
                    type=str,
                    help='the input file')

# parser.add_argument('AveragingWindowSize',
#                     metavar='windowSize',
#                     type=int,
#                     help='the size of the averaging window')

# Add autocomplete
argcomplete.autocomplete(parser)

# Execute the parse_args() method
args = parser.parse_args()

# Get the input file
input_file_path = Path(args.InputFile)

if not input_file_path.exists:
    print(f'File does not exist at: {input_file_path.resolve()}')
    sys.exit()

with open(input_file_path.resolve(), 'r') as l:
    
    lines = l.readlines()

    aim = 0
    depth = 0
    forward = 0

    for l in lines:
        if l.find("up") != -1 and aim >= 0:
            aim -= int(str(l).split()[1])
        if l.find("down") != -1:
            aim += int(str(l).split()[1])
        if l.find("forward") != -1:
            depth += aim * int(str(l).split()[1])
            forward += int(str(l).split()[1])
    
    print(f'Result: {forward * depth}')
        
    # averaging_windows = [lines[i:(i+window_size)] for i in range(len(lines)) if len(lines[i:(i+window_size)]) == window_size]
    
    # averages = [sum(averaging_windows[i]) for i in range(len(averaging_windows))]
    
    # for v in averages:
    #     # first iteration
    #     if lastLine is None:
    #         lastLine = int(v)
    #         continue
        
    #     if int(v) > lastLine:
    #         largerThanPreviousMeasurementsCount += 1 
            
    #     lastLine = int(v)
            
    # print(f'Larger than previous count: {largerThanPreviousMeasurementsCount}')
