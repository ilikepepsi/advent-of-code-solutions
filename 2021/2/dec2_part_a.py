#!/usr/bin/python

import argcomplete, argparse
import sys

from pathlib import Path

def sum(values: list):
    sum = 0
    for v in values:
        sum += int(v)
    return sum

# Create the parser
parser = argparse.ArgumentParser(description="Specify the input file.")

# Add the arguments
parser.add_argument('InputFile',
                    metavar='inputFile',
                    type=str,
                    help='the input file')

# parser.add_argument('AveragingWindowSize',
#                     metavar='windowSize',
#                     type=int,
#                     help='the size of the averaging window')

# Add autocomplete
argcomplete.autocomplete(parser)

# Execute the parse_args() method
args = parser.parse_args()

input_file_path = Path(args.InputFile)
# window_size = args.AveragingWindowSize

if not input_file_path.exists:
    print(f'File does not exist at: {input_file_path.resolve()}')
    sys.exit()

#def align_data_to_window(data: list, window_size: int) -> list:
#    return data[:-(len(data)-len(data)%window_size)]

with open(input_file_path.resolve(), 'r') as f:
    
    # largerThanPreviousMeasurementsCount = 0
    # lastLine = None
    
    lines = f.readlines()
    
    forward = [str(f).split()[1] for f in lines if f.find("forward") != -1]
    up = [str(f).split()[1] for f in lines if f.find("up") != -1]
    down = [str(f).split()[1] for f in lines if f.find("down") != -1]
    
    print(f'Result: {sum(forward) * (sum(down) - sum(up))}')
        
    # averaging_windows = [lines[i:(i+window_size)] for i in range(len(lines)) if len(lines[i:(i+window_size)]) == window_size]
    
    # averages = [sum(averaging_windows[i]) for i in range(len(averaging_windows))]
    
    # for v in averages:
    #     # first iteration
    #     if lastLine is None:
    #         lastLine = int(v)
    #         continue
        
    #     if int(v) > lastLine:
    #         largerThanPreviousMeasurementsCount += 1 
            
    #     lastLine = int(v)
            
    # print(f'Larger than previous count: {largerThanPreviousMeasurementsCount}')