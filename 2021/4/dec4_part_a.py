#!/usr/bin/python

import argcomplete, argparse
import sys

from pathlib import Path

def sum(values: list):
    sum = 0
    for v in values:
        sum += int(v)
    return sum


def inverse(bits: list):
    inverse_s = ''

    for i in bits:
        if i == '0':
            inverse_s += '1'
        else:
            inverse_s += '0'

    return inverse_s


def list_to_string(chars: list):
    result = ''

    for c in chars:
        result += c

    return result


def binary_to_decimal(bits: str):
    result = 0
    exp = 0
    for b in bits[::-1]:
        result += int(b) * 2 ** exp
        exp += 1

    return result

# Create the parser
parser = argparse.ArgumentParser(description="Specify the input file.")

# Add the arguments
parser.add_argument('InputFile',
                    metavar='inputFile',
                    type=str,
                    help='the input file')

# Add autocomplete
argcomplete.autocomplete(parser)

# Execute the parse_args() method
args = parser.parse_args()

input_file_path = Path(args.InputFile)

if not input_file_path.exists:
    print(f'File does not exist at: {input_file_path.resolve()}')
    sys.exit()

with open(input_file_path.resolve(), 'r') as f:

    lines = f.readlines()
    
    random_number_input = lines[0].split(',')
    
    slice_indexes = [i for i in range(len(lines[2:])) if lines[i+2] == '\n']
    
    bingo_cards = []
    slice_buffer = 0
    
    for i in range(len(slice_indexes)):
        if i == 0:
            bingo_cards.append(lines[2+slice_buffer:2+slice_indexes[i]])
        else:
            bingo_cards.append(lines[2+slice_buffer+1:2+slice_indexes[i]])
    slice_buffer = slice_indexes[i]
    
    pass       